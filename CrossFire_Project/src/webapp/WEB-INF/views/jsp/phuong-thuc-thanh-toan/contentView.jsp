<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>

  <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Thông tin tài khoản CF
   </h1>
   <ol class="breadcrumb">
     <li><a href="#"><i class="fa fa-dashboard"></i> Trang Chủ</a></li>
     <li class="active">Thông tin tài khoản CF</li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">
   <div class="row">

     <div class="col-xs-12">
       <div class="box">
         <div class="box-header">
           <button class="btn btn-block btn-success" style="width: 90px;margin-top: 8px; text-align: left;">Thêm Mới</button>
         </div><!-- /.box-header -->
         <div class="box-body">
           <table id="example1" class="table table-bordered table-striped">
             <thead>
               <tr>
                 <th>Loại Tài Khoản</th>
                 <th>Mã Tài Khoản</th>
                 <th>Cấp VIP Ingame</th>
                 <th>Số VIP</th>
                 <th>Thông Tin Tài Khoản</th>
                 <th>Giá Tài Khoản</th>
                 <th>Tổng số ảnh</th>
                 <th>Thực hiện</th>
               </tr>
             </thead>
             <tbody>
               <tr>
                 <td>Trident</td>
                 <td>Internet
                   Explorer 4.0</td>
                 <td>Win 95+</td>
                 <td> 4</td>
                 <td>Win 95+</td>
                 <td> 4</td>
                 <td> 4</td>
                 <td>
                   <button type="button" class="btn btn-info">Chỉnh Sửa</button>
                   <button type="button" class="btn btn-info" style="background-color: #dd4b39">Xóa</button>
                 </td>
               </tr>
               <tr>
                 <td>Other browsers</td>
                 <td>All others</td>
                 <td>-</td>
                 <td>-</td>
                 <td>Win 95+</td>
                 <td> 4</td>
                 <td> 4</td>
                 <td>
                   <button type="button" class="btn btn-info">Chỉnh Sửa</button>
                   <button type="button" class="btn btn-info" style="background-color: #dd4b39">Xóa</button>
                 </td>
               </tr>
               <tr>
                 <td>Other browsers</td>
                 <td>All others</td>
                 <td>-</td>
                 <td>-</td>
                 <td>Win 95+</td>
                 <td> 4</td>
                 <td> 4</td>
                 <td>
                   <button type="button" class="btn btn-info">Chỉnh Sửa</button>
                   <button type="button" class="btn btn-info" style="background-color: #dd4b39">Xóa</button>
                 </td>
               </tr>
             </tbody>
           </table>
         </div><!-- /.box-body -->
       </div><!-- /.box -->
     </div><!-- /.col -->
   </div><!-- /.row -->
 </section><!-- /.content -->