package common;

public class ExceptionID {
	
	// khai bao exceptionID
	public static String EU0001 = "EU0001";
	public static String MU0001 = "MU0001";
	public static String MU0002 = "MU0002";
	public static String MU0003 = "MU0003";
	public static String MU0004 = "MU0004";
	public static String MU0005 = "MU0005";
	public static String EPD001 = "EPD001";
	public static String MPD001 = "MPD001";
	public static String EPD002 = "EPD002";
	public static String MPD002 = "MPD002";
	public static String MPD003 = "MPD003";
	public static String MPD004 = "MPD004";
	public static String EWL001 = "EWL001";
	public static String MWL001 = "MWL001";
	public static String EWL002 = "EWL002";
	public static String MWL002 = "MWL002";
	public static String MWL003 = "MWL003";
	public static String MWL004 = "MWL004";
	public static String SH0001 = "SH0001";
	public static String MSH0001 = "MSH0001";
}
