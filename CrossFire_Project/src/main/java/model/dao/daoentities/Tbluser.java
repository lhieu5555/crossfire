package model.dao.daoentities;
// default package
// Generated Mar 31, 2021 11:45:49 PM by Hibernate Tools 4.3.5.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Tbluser generated by hbm2java
 */
@Entity
@Table(name = "tbluser", catalog = "crossfire_db")
public class Tbluser implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TbluserId id;
	private String userPassword;
	private String userCreateDate;
	private String userUpdateDate;
	private Boolean deleteFlag;
	private String userEmail;
	private String userVerifyCode;
	private String userVerifyCodeCreateDate;
	private String userVerifyCodeExpriedDate;

	public Tbluser() {
	}

	public Tbluser(TbluserId id, String userPassword, String userEmail) {
		this.id = id;
		this.userPassword = userPassword;
		this.userEmail = userEmail;
	}

	public Tbluser(TbluserId id, String userPassword, String userCreateDate, String userUpdateDate, Boolean deleteFlag,
			String userEmail, String userVerifyCode, String userVerifyCodeCreateDate, String userVerifyCodeExpriedDate) {
		this.id = id;
		this.userPassword = userPassword;
		this.userCreateDate = userCreateDate;
		this.userUpdateDate = userUpdateDate;
		this.deleteFlag = deleteFlag;
		this.userEmail = userEmail;
		this.userVerifyCode = userVerifyCode;
		this.userVerifyCodeCreateDate = userVerifyCodeCreateDate;
		this.userVerifyCodeExpriedDate = userVerifyCodeExpriedDate;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "userId", column = @Column(name = "USER_ID", nullable = false)),
			@AttributeOverride(name = "userName", column = @Column(name = "USER_NAME", nullable = false, length = 15)) })
	public TbluserId getId() {
		return this.id;
	}

	public void setId(TbluserId id) {
		this.id = id;
	}

	@Column(name = "USER_PASSWORD", nullable = false, length = 100)
	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "USER_CREATE_DATE", length = 19)
	public String getUserCreateDate() {
		return this.userCreateDate;
	}

	public void setUserCreateDate(String userCreateDate) {
		this.userCreateDate = userCreateDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "USER_UPDATE_DATE", length = 19)
	public String getUserUpdateDate() {
		return this.userUpdateDate;
	}

	public void setUserUpdateDate(String userUpdateDate) {
		this.userUpdateDate = userUpdateDate;
	}

	@Column(name = "DELETE_FLAG")
	public Boolean getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	@Column(name = "USER_EMAIL", nullable = false, length = 50)
	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	@Column(name = "USER_VERIFY_CODE", length = 100)
	public String getUserVerifyCode() {
		return this.userVerifyCode;
	}

	public void setUserVerifyCode(String userVerifyCode) {
		this.userVerifyCode = userVerifyCode;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "USER_VERIFY_CODE_CREATE_DATE", length = 19)
	public String getUserVerifyCodeCreateDate() {
		return this.userVerifyCodeCreateDate;
	}

	public void setUserVerifyCodeCreateDate(String userVerifyCodeCreateDate) {
		this.userVerifyCodeCreateDate = userVerifyCodeCreateDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "USER_VERIFY_CODE_EXPRIED_DATE", length = 19)
	public String getUserVerifyCodeExpriedDate() {
		return this.userVerifyCodeExpriedDate;
	}

	public void setUserVerifyCodeExpriedDate(String userVerifyCodeExpriedDate) {
		this.userVerifyCodeExpriedDate = userVerifyCodeExpriedDate;
	}

}
